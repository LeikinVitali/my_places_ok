////
////  RatingControl.swift
////  MyPlaces
////
////  Created by admin on 18.01.21.
////
//
//import UIKit
//
//@IBDesignable class RatingControl: UIStackView {
//
//    private var ratingButtons = [UIButton]()
//    var rating = 0{
//        didSet{
//            updateButtonSelectionState()
//        }
//
//    }
//
//    @IBInspectable var starSize: CGSize = CGSize(width: 44.0, height: 44.0){
//        didSet{
//            setupButtons()
//        }
//    }
//    @IBInspectable var starCount: Int = 5{
//        didSet{
//            setupButtons()
//        }
//    }
//    //MARK: - Initialization
//
//    override init(frame: CGRect){
//        super.init(frame: frame)
//    }
//
//    required init(coder: NSCoder) {
//        super.init(coder: coder)
//        setupButtons()
//    }
//
//    @objc func ratingButtonTap(button: UIButton){
//
//        guard let index = ratingButtons.firstIndex(of: button) else{return}
//
//        // Calculate the rating
//
//        let selectedRating = index + 1
//
//        if selectedRating == rating{
//            rating = 0
//        } else{
//            rating = selectedRating
//        }
//
//    }
//
//    private func setupButtons(){
//
//        for button in ratingButtons{
//            removeArrangedSubview(button)
//            button.removeFromSuperview()
//        }
//        ratingButtons.removeAll()
//
//        // load button image
//        let bundle = Bundle(for: type(of: self))
//        let fieldStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
//        let emptyStar = UIImage(named: "emptyStar", in: bundle, compatibleWith: self.traitCollection)
//        let highLightStat = UIImage(named: "highlightedStar", in: bundle, compatibleWith: self.traitCollection)
//
//
//
//        for _ in 0..<starCount{
//            let button = UIButton()
//            //Set the button image
//
//            button.setImage(emptyStar, for: .normal)
//            button.setImage(fieldStar, for: .selected)
//            button.setImage(highLightStat, for: .highlighted)
//            button.setImage(highLightStat, for: [.highlighted, .selected])
//
//
//            // Add constraints
//            button.translatesAutoresizingMaskIntoConstraints = false
//            button.heightAnchor.constraint(equalToConstant: starSize.height).isActive = true
//            button.widthAnchor.constraint(equalToConstant: starSize.width).isActive = true
//
//            button.addTarget(self, action: #selector(ratingButtonTap(button:)), for: .touchUpInside)
//            addArrangedSubview(button)
//
//            ratingButtons.append(button)
//        }
//        updateButtonSelectionState()
//        }
//
//
//
//    private func updateButtonSelectionState(){
//        for(index, button) in ratingButtons.enumerated(){
//            button.isSelected = index < rating
//        }
//    }
//
//}
