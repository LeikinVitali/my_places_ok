//
//  MainViewController.swift
//  MyPlaces
//
//  Created by admin on 17.01.21.
//

import UIKit
import RealmSwift
class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var places: Results<Place>!
    private var filteredPlaces: Results<Place>!
    private var ascendingSorting = true
    private var searchBarIsEmpty: Bool?{
        guard let text = searchController.searchBar.text else{return false}
        return text.isEmpty
    }
    private var isFiltering: Bool{
        return searchController.isActive && !searchBarIsEmpty!
    }
    
    
    @IBOutlet weak var reversedSortingButton: UIBarButtonItem!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        places = realm.objects(Place.self)
        
        // Setup searchController
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }

    
    
    
    // MARK: - Table view data source


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering{
            return filteredPlaces.count
        }
        return self.places.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell

//        var place = Place()
//     //   let place = places[indexPath.row]
//
//        if isFiltering{
//            place = filteredPlaces[indexPath.row]
//        }else{
//            place = places[indexPath.row]
//        }
        
        let place = isFiltering ? filteredPlaces[indexPath.row] : places[indexPath.row]
        cell.nameLabel?.text = place.name
        cell.locationLabel.text = place.location
        cell.typeLabel.text = place.type
        cell.imageOfPlace.image = UIImage(data: place.imageData!)
        
        cell.cosmosView.rating = place.rating
        
        return cell
    }

    
    // MARK: - Table View Delegate
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        //удаление строк таблицы
        let place = places[indexPath.row]
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete") { (_, _) in
            StorageManager.deleteObject(place)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        return [deleteAction]
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    
    
   //  MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail"{
            guard let indexPath = tableView.indexPathForSelectedRow else{return}
//            let place: Place
//            if isFiltering{
//                place = filteredPlaces[indexPath.row]
//            }else{
//                place = places[indexPath.row]
//            }
            
            let place = isFiltering ? filteredPlaces[indexPath.row] : places[indexPath.row]
//let place = places[indexPath.row]
            let newPlaceVC = segue.destination as! NewPlaceViewController
            newPlaceVC.currentPlace = place
        }
    }
    
    
    @IBAction func unwinSegue(_ segue: UIStoryboardSegue){
        guard let newPlaceVC = segue.source as? NewPlaceViewController else {return}
        newPlaceVC.savePlace()        
        tableView.reloadData()
    }
    
    // сортировка данных
    
    @IBAction func sortSelection(_ sender: UISegmentedControl) {

        self.sorting()
    }
    
    @IBAction func reversedSorting(_ sender: Any) {
        
        self.ascendingSorting.toggle()
        
        if self.ascendingSorting{
            self.reversedSortingButton.image = #imageLiteral(resourceName: "AZ")
        }else{
            self.reversedSortingButton.image = #imageLiteral(resourceName: "ZA")
        }
        
        self.sorting()
    }
    
    
    private func sorting(){
        if segmentControl.selectedSegmentIndex == 0 {
            places = places.sorted(byKeyPath: "date", ascending: ascendingSorting)
        }else{
            places = places.sorted(byKeyPath: "name", ascending: ascendingSorting)
        }
        
        tableView.reloadData()
    }
    
}


extension MainViewController: UISearchResultsUpdating{
  
    func updateSearchResults(for searchController: UISearchController) {
        self.filterContentForSearchText(searchController.searchBar.text!)
    }
    
    
    private func filterContentForSearchText(_ searchText: String){
        filteredPlaces = places.filter("name CONTAINS[c] %@ OR location CONTAINS[c] %@", searchText, searchText )
        tableView.reloadData()
    }
    
    
}
